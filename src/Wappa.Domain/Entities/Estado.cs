﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wappa.Domain.Entities
{
    public class Estado
    {
        public int EstadoId { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }
    }
}
