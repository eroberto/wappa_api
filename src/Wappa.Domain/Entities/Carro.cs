﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wappa.Domain.Entities
{
    public class Carro
    {
        public int CarroId { get; set; }
        public string Marca { get; set; }
        public string Placa { get; set; }
    }
}
