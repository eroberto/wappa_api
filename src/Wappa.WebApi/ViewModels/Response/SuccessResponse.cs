﻿using System.Linq;
using Wappa.Domain.Entities;
using Wappa.WebApi.ViewModels.Common;

namespace Wappa.WebApi.ViewModels.Response
{
    public class StatusResponse
    {
        public bool Status { get { return true; } }        
    }
}
