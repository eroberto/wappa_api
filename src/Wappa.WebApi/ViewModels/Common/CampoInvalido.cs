﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wappa.WebApi.ViewModels.Common
{
    public class CampoInvalido
    {
        public string Campo { get; set; }

        public string Mensagem { get; set; }
    }
}
