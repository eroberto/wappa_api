## Instalação

Clone o projeto e rode o docker:

    $ git clone git@bitbucket.org:eroberto/wappa_api.git
    $ cd wappa_api/src
    $ docker-compose up

Aguarde a sincronização das duas imagens (web e mysql) para poder executar o projeto.

A documentação pode ser acessada no arquivo swagger.yml que está na pasta /src. 

O projeto rodará na porta 8081.

### Tempo gasto, dificuldades e problemas.

Recebi o teste na quinta-feira à noite (29/11). Eu gastei em média 12 horas para fazer o projeto. Comecei no sábado (02/12) por volta das 10h da manhã, e alternando com alguns afazeres do fim de semana, finalizei na segunda-feira de madrugada (1h da manhã).

Procurei fazer o uso do Dapper, por ser um ORM leve e stored procedures para operar com o banco. Embora eu tenha perdido algunas funcionalidades que um ORM tradicional tem, como a facilidade em fazer um CRUD, por outro lado essa solução torna o projeto mais limpo e independente de anti-patterns que os ORMs nos fazem programar. Existe uma discussão interessante sobre o assunto, sintetizado pelo Martin Fowler nesse artigo: https://martinfowler.com/bliki/OrmHate.html

Usei uma arquitetura que lembra o Domain Driven Design. Alguns chamam de "DDD Lite". No entanto, eu vejo o DDD mais como uma filosofia de desenvolvimento do que um framework. Implementei algumas ideias utilizadas como isolar a camada de dominio da infraestrutura, a criação de serviços e etc. O Dapper se mostrou um impeditivo na aplicação de um Rich Domain, fazendo com que eu utilizasse classes anêmicas. Esse será, inclusive, um ponto que estudarei no futuro: "Como implementar um modelo rico usando o Dapper". Procurei na internet e, infelizmente, todo mundo está fazendo POCO com modelo anêmico. =/

Configurei o Docker para facilitar no desenvolvimento e para servir como uma infra para testar. O Dockerbuild default do Visual Studio me pareceu um pouco inútil. Tive que adaptá-la para poder funcionar como queira, no entanto, não configurei o Dockerize para fazer as duas imagens (MySql e dotnet) subirem sincronizada. Ficaria como um to-do, se fosse um projeto real.

Na minha primeira tentativa de criar o projeto eu fiz usando TDD (conforme o livro do Kent Back). Estava implementando um domínio rico. Tive a infelicidade de ter problemas com o Dapper, porque ele causa um forte acoplamento forçando que eu cria os meus construturoes para que funcionem da maneira que ele bem entender. Por causa disso, eu voltei atrás e os testes, embore alguns funcionem, não estão 100% implementados.

Resolvi não usar migration. Os arquivos de migração seriam SQLs puros, que facilitam o uso para o caso de eu precisar rodar direto no banco de dados. Caso fosse utilizar, gosto do framework MigSharp.

A parte da WebAPI, eu considero que poderia melhorar muito. A base de tudo está pronta. Falta eu fazer mais algumas validações, melhorar alguns retornos e fazer a paginação. Infelizmente, não deu tempo. 